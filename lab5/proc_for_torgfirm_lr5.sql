﻿use torgfirm;


--1.	Розробити процедуру для отримання назв і вартості товарів, проданих визначеним співробітником.
CREATE PROC lab7_proc1 AS
SELECT Tovar.Nazva_t, tovar.Price*zakaz_tovar.kilk AS Vartist, sotrudnik.Fname, sotrudnik.Name 
	FROM Sotrudnik 
	INNER JOIN zakaz ON sotrudnik.id_sotrudnik=zakaz.k_sotrud 
	inner join zakaz_tovar on zakaz.K_zakaz=zakaz_tovar.id_zakaz 
	INNER JOIN tovar ON tovar.id_tovar=zakaz_tovar.id_tovar
WHERE sotrudnik.Name like 'Иван'
--Виклик процедури
exec lab7_proc1

--2.	Створити процедуру для зменшення ціни товару постачальника ПП "Арей" на 10%.

--select * from tovar
--WHERE tovar.id_postach=(SELECT postachalnik.id_postach 
--					FROM postachalnik 
--						WHERE postachalnik.Nazva like 'ТОВ СМ')

CREATE PROC lab7_proc2 AS
UPDATE tovar SET Price=Price*0.9
	WHERE tovar.id_postach=(SELECT postachalnik.id_postach 
					FROM postachalnik 
						WHERE postachalnik.Nazva like 'ТОВ СМ')
--Виклик процедури
exec lab7_proc2


--3.	Створити процедуру для отримання назв і вартості товарів, які придбав заданий клієнт.
--select * from klient

CREATE PROC lab7_proc3 @k VARCHAR(20)
AS
SELECT tovar.id_tovar, tovar.Price*zakaz_tovar.kilk AS vartist, klient.Nazva 
	FROM klient INNER JOIN ((tovar INNER JOIN zakaz_tovar 
ON tovar.id_tovar=zakaz_tovar.id_tovar) inner join zakaz 
on zakaz.K_zakaz=zakaz_tovar.id_zakaz) 
ON klient.id_klient=zakaz.K_klien
WHERE klient.Nazva=@k

--Виклик процедури
--1)
exec lab7_proc3 'ТОВ Груп'
--2)
declare @k varchar(20) = 'ТОВ Груп'
exec lab7_proc3 @k

--4.	Створити процедуру для зменшення ціни товару з певним запасом відповідно до вказаного %.
--select * from tovar

CREATE PROC lab7_proc4
@t VARCHAR(20), @p FLOAT 
AS
UPDATE tovar SET Price=Price*(1 -@p/100) 
WHERE tovar.Min_zap=@t

--Виклик процедури
--1)
exec lab7_proc4 @t=4, @p=5
--2)
exec lab7_proc4 4, 5

--5.	Створити процедуру для зменшення ціни товару заданого типу відповідно до вказаного %.
--select * from tovar

--UPDATE tovar SET Price=23200
--WHERE tovar.Nazva_t='Айфон Х'

CREATE PROC lab7_proc5
@t VARCHAR(20)='Айфон Х', @p FLOAT=1 
AS
UPDATE tovar SET Price=Price*(1 -@p/100)
WHERE tovar.Nazva_t=@t

--Виклик процедури
--1)
exec lab7_proc5 'Айфон Х', 5
--2)
exec lab7_proc5  @p=2
exec lab7_proc5 'Айфон Х'
--3)
exec lab7_proc5 

--6.	Створити процедуру для визначення загальної вартості товарів, проданих за конкретний місяць.
--select * from zakaz
--update zakaz set zakaz.date_naznach = '2018-04-26' where zakaz.K_zakaz=1

CREATE PROC lab7_proc6 
@m INT, @s FLOAT OUTPUT 
AS
SELECT @s=Sum(tovar.Price*zakaz_tovar.kilk)
	FROM tovar INNER JOIN zakaz_tovar ON tovar.id_tovar=zakaz_tovar.id_tovar 
				inner join zakaz on zakaz.K_zakaz=zakaz_tovar.id_zakaz
	GROUP BY Month(zakaz.date_naznach)
	HAVING Month(zakaz.date_naznach)=@m

--Виклик процедури
declare @s FLOAT
exec lab7_proc6 4, @s OUTPUT
print @s

--7.	Створити процедуру для визначення загальної кількості товарів, придбаних фірмами в місті, де розташоване визначене підприємство.
--Спочатку розробимо процедуру для визначення міста, де працює клієнт. 

--select * from klient

CREATE PROC lab7_subproc7
@f VARCHAR(20) OUTPUT, @n VARCHAR(20)
 AS
SELECT @f=City FROM klient WHERE Nazva=@n

--Виклик процедури
declare @f varchar(20)
exec lab7_subproc7 @f output, @n = 'ФОП Ильенко'
Print @f

--Використання вкладених процедур. Потім створимо процедуру, що підраховує загальну вартість замовлень з даного міста.

Create proc lab7_proc7
@suma float output, @n varchar(20)
as
declare @pr varchar(20)
exec lab7_subproc7 @pr output, @n
select @suma=(tovar.Price*zakaz_tovar.kilk)
from zakaz inner join zakaz_tovar on zakaz.K_zakaz = zakaz_tovar.id_zakaz
inner join tovar on zakaz_tovar.id_tovar = tovar.id_tovar
where zakaz.K_klien IN (select K_klien from klient
where city = @pr)

--Виклик процедури
declare @f varchar(20)
exec lab7_proc7 @f output, @n = 'ФОП Ильенко'
Print @f 
