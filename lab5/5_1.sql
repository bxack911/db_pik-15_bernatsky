CREATE PROC lab5_1 AS
Declare  @pib varchar(50),
         @course varchar(50),
		 @suma varchar(50)

Declare st cursor For

SELECT student.PIB,course.Nazva,cost.Suma
	FROM student
	INNER JOIN course ON student.id_course=course.id_course
	INNER JOIN cost ON course.id_cost=cost.id_cost
WHERE cost.Suma BETWEEN 9100 AND 10000;

OPEN st 
Fetch Next From st Into @pib, @course, @suma

While @@Fetch_Status = 0 Begin

    print 'PIB: ' + @pib
    print 'Course: ' + @course
	print 'Sum: ' + @suma


Fetch Next From st Into @pib, @course, @suma

End 

Close st
Deallocate st

exec lab5_1