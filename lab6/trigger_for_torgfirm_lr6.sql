﻿use torgfirm;

--1. Використання тригера для реалізації обмежень на значення. 
--У записі, що додається в таблицу Zakaz_tovar, кількість проданого 
--товару має бути не більше, ніж його залишок з таблиці Склад.	

INSERT INTO zakaz_tovar(id_zakaz, id_tovar, Kilk, Znigka)
VALUES (4,1,25, 0.05)

CREATE TRIGGER lab7_oneRow ON zakaz_tovar FOR INSERT AS
IF @@ROWCOUNT=1 BEGIN
IF NOT EXISTS(
SELECT * FROM inserted
		WHERE inserted.Kilk<=ALL(SELECT tovar.Zap_skl
		FROM tovar, zakaz_tovar WHERE tovar.id_tovar= zakaz_tovar.id_tovar))
BEGIN
ROLLBACK TRAN PRINT 'Нема товару'
END
END

--2. При продажі або отриманні товару необхідно відповідним чином 
--змінити кількість його складського запасу. Якщо товару на складі 
--ще немає, необхідно про це повідомити. Тригер обробляє тільки один 
--рядок, що додається.

CREATE TRIGGER trig_inst ON zakaz_tovar FOR INSERT AS
DECLARE @x INT, @y INT IF @@ROWCOUNT=1 
BEGIN
IF NOT EXISTS(SELECT * 
	FROM inserted 
	WHERE - inserted.Kilk<=ALL(SELECT tovar.Zap_skl
	FROM tovar, zakaz_tovar 
	WHERE tovar.id_tovar= zakaz_tovar.id_tovar))
BEGIN
ROLLBACK TRAN PRINT 'Нестача товару'
END
IF NOT EXISTS ( SELECT i.id_tovar, i.Kilk, 2 FROM tovar N, inserted i 
WHERE N.id_tovar =i.id_tovar)
print 'Товар не продається'
	ELSE BEGIN
		SELECT @y=i.id_tovar, @x=i.Kilk 
			FROM zakaz_tovar N, inserted i 
			WHERE N.id_tovar=i.id_tovar 
		UPDATE tovar SET Zap_skl=Zap_skl-@x WHERE id_tovar=@y
		print 'Ok'
	END
END

--3. Створити тригер для обробки операції видалення запису з таблиці 
--Zakaz_tovar, наприклад, такої команди:

DELETE FROM Zakaz_tovar WHERE id_zakaz=4 

CREATE TRIGGER lab7_del ON zakaz_tovar FOR DELETE 
AS
IF @@ROWCOUNT=1 
BEGIN DECLARE @y INT,@x INT 
SELECT @y=id_tovar, @x=Kilk FROM deleted 
UPDATE tovar SET Zap_skl=Zap_skl+@x WHERE id_tovar=@y
END

--4. Створити тригер для обробки операції зміни запису в таблиці, 
--наприклад, такою командою:

UPDATE zakaz_tovar SET Kilk=Kilk - 10 WHERE id_tovar=3 

CREATE TRIGGER trig_upd
ON zakaz_tovar INSTEAD OF UPDATE
AS
DECLARE @k INT, @k_old INT DECLARE @x INT, @x_old INT, @y 
INT DECLARE @y_old INT,@o INT DECLARE CUR1 CURSOR FOR 
SELECT id_zakaz, id_tovar, kilk FROM inserted
DECLARE CUR2 CURSOR FOR SELECT id_zakaz, id_tovar, kilk 
FROM deleted OPEN CUR1 OPEN CUR2
	FETCH NEXT FROM CUR1 INTO @k,@x, @y 
	FETCH NEXT FROM CUR2 INTO @k_old,@x_old, @y_old
	WHILE @@FETCH_STATUS=0 
		BEGIN
			SELECT @o=Zap_skl 
			FROM tovar WHERE 
			id_tovar=@x IF @o>=-@y 
			BEGIN
			RAISERROR('зміна ', 16,10)
			UPDATE zakaz_tovar SET kilk=@y, id_tovar=@x 
			WHERE id_zakaz=@k 
			UPDATE tovar
			SET Zap_skl=Zap_skl-@y_old WHERE 
			id_tovar=@x_old
			IF NOT EXISTS (SELECT * FROM tovar 
				WHERE id_tovar=@x)
			INSERT INTO tovar(id_tovar, Zap_skl) 
				VALUES (@x,@y)
			ELSE
			UPDATE tovar SET Zap_skl=Zap_skl+@y 
			WHERE id_tovar=@x END ELSE
				RAISERROR('запис не змінено', 16,10)
			FETCH NEXT FROM CUR1 INTO @k,@x, @y 
			FETCH NEXT FROM CUR2 INTO @k_old,@x_old, @y_old
		END
	CLOSE CUR1 
	CLOSE CUR2 
	DEALLOCATE CUR1 
	DEALLOCATE CUR2

