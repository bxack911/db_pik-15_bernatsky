CREATE TRIGGER on_count ON teacher 
FOR INSERT AS
IF (SELECT count(*) FROM teacher) < 4
BEGIN
DECLARE @id int,
		@pib varchar(50)
SELECT @id=id_teacher,  @pib=PIB FROM inserted
INSERT INTO teacher(id_teacher,PIB) VALUES (@id,@pib);
END
ELSE IF (SELECT count(*) FROM teacher) > 4
BEGIN
ROLLBACK TRAN
PRINT 'Количество записей превышает дорустимое'
END