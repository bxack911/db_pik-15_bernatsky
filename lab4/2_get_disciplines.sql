--�������� ������� �������� � ����� ���������.--
CREATE VIEW get_disciples AS
SELECT dbo_student.Name,dbo_groups.Kod_group,
Navch_plan.K_navch_plan,Predmet_plan.K_predmet,predmet.Nazva
FROM (dbo_student INNER JOIN dbo_groups ON dbo_student.Kod_group=dbo_groups.Kod_group)
	 INNER JOIN Navch_plan ON Navch_plan.K_navch_plan=dbo_groups.K_navch_plan
	 INNER JOIN Predmet_plan ON Predmet_plan.K_navch_plan=Navch_plan.K_navch_plan
	 INNER JOIN predmet ON predmet.K_predmet=Predmet_plan.K_predmet
GROUP BY dbo_student.Name,dbo_groups.Kod_group,
		 Navch_plan.K_navch_plan,Predmet_plan.K_predmet,predmet.Nazva