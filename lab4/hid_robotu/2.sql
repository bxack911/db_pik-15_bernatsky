CREATE VIEW qresult2 AS
SELECT QResult.id_zakaz,klient.Nazva,zakaz.date_naznach, SUM(QResult.Zag_vartist) AS Itog
FROM (klient INNER JOIN zakaz ON klient.id_klient=zakaz.id_klient) INNER JOIN QResult ON zakaz.id_zakaz=QResult.id_zakaz
GROUP BY QResult.id_zakaz,klient.Nazva,zakaz.date_naznach;