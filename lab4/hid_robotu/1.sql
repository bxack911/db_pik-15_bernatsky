CREATE VIEW qresult AS
SELECT zakaz_tovar.id_zakaz,klient.Nazva,zakaz_tovar.id_tovar,zakaz_tovar.kilkist,zakaz_tovar.Znigka,Tovar.Price*zakaz_tovar.kilkist*(1-zakaz_tovar.Znigka) AS Zag_vartist
FROM (klient INNER JOIN zakaz ON klient.id_klient = zakaz.id_klient) INNER JOIN (Tovar INNER JOIN zakaz_tovar ON Tovar.id_tovar = zakaz_tovar.id_tovar) ON zakaz.id_zakaz = zakaz_tovar.id_zakaz;