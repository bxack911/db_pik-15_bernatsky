SELECT sotrudnik.Fname,sotrudnik.Name,sotrudnik.Posada,AVG(QResult2.Itog) AS [AVG-Itog]
FROM (sotrudnik INNER JOIN zakaz ON sotrudnik.id_sotrud=zakaz.id_sotrud)
INNER JOIN QResult2 ON zakaz.id_zakaz=QResult2.id_zakaz
GROUP BY sotrudnik.Fname,sotrudnik.Name,sotrudnik.Posada;